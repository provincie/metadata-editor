# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from ME_19115_131_ui import Ui_ME_19115_131
from PIL import Image
from StringIO import StringIO

import os, uuid, requests, tempfile

# ----------------------------------------------------------------------

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

# ----- CONFIG CLASS ---------------------------------------------------

class Config:
  """
  Lees config bestand en haal key's op
  Schrijf dictionarie naar config
  """
  def __init__(self, conf_bestand):
    """
    ini config object
    """
    self.conf_bestand = conf_bestand
    self.conf = None

  def get(self, key, default = None):
    """
    Lees values uit config bestand
    """    
    if not self.conf: self.load()
    if self.conf and (key in self.conf): return self.conf[key]
    return default

  def get_dict(self, default = None):
    """
    Geef de complete dictionarie
    """    
    if not self.conf: self.load()
    if self.conf: return self.conf
    return default

  def set(self, key, value):
    """
    Voeg keys en values toe aan config 
    """    
    if not self.conf: self.load()
    self.conf[key] = value
    self.save()

  def load(self):
    """
    Laad het config bestand
    Als het niet bestaat maak een lege config
    """    
    try: self.conf = eval(open(self.conf_bestand, 'r').read())
    except: self.conf = {}

  def save(self):
    """
    Schijf dictionarie naar config bestand
    """    
    open(self.conf_bestand, 'w').write(repr(self.conf))
# ----- Test gebied ----------------------------------------------------

    bericht = str(self.iface.activeLayer().name())
    # als het een vector laag en erkomt een pipe voor
    if self.iface.activeLayer().type() == 0 and '|' in self.iface.activeLayer().dataProvider().dataSourceUri():
      self.xml_map, bestand = os.path.split(self.iface.activeLayer().dataProvider().dataSourceUri().split('|')[0])
      layername
      bericht += '\n\nmap: %s\nbestand: %s' %(self.xml_map, bestand)
    elif self.iface.activeLayer().type() == 0 and 'dbname' in self.iface.activeLayer().dataProvider().dataSourceUri():
      laag_geg = {item.split('=')[0]: item.split('=')[1].replace('"', '').replace("'", "") for item in self.iface.activeLayer().dataProvider().dataSourceUri().split(' ') if '=' in item}
      bericht += '\n\ndatabase: %s\ntabel: %s' %(laag_geg['dbname'], laag_geg['table'])
    elif self.iface.activeLayer().type() == 1:
      best_map, bestand = os.path.split(self.iface.activeLayer().dataProvider().dataSourceUri())
      bericht += '\n\nmap: %s\nbestand: %s' %(best_map, bestand)


    bericht += '\n'+self.iface.activeLayer().dataProvider().dataSourceUri()
    # ~ bericht += '\n'+self.iface.activeLayer().dataProvider().tr()
    bericht += '\n\n'+self.eigenlijsten["mapunits"][self.iface.activeLayer().crs().mapUnits()]
    if self.iface.activeLayer().type() == 0:
      bericht += '\n\n'+str(self.iface.activeLayer().fields().names())
      bericht += '\n'+str(self.iface.activeLayer().dataProvider().storageType())

    QtWidgets.QMessageBox.warning(None, "Actieve laag", bericht)
      
    # ~ overzicht = str(dir(self.iface.activeLayer().extent()))
    # ~ with open(self.parent.start_dir+os.sep+'test'+os.sep+'actieve_laag_extent.txt', 'w') as overzichtfile: overzichtfile.write(overzicht)

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
    
    bericht = '\n\nxml_map: %s\nxml_bestand: %s' %(self.xml_map, self.xml_naam)
    QtWidgets.QMessageBox.warning(None, "Actieve laag", bericht)
      
    # ~ overzicht = str(dir(self.iface.activeLayer().extent()))
    # ~ with open(self.parent.start_dir+os.sep+'test'+os.sep+'actieve_laag_extent.txt', 'w') as overzichtfile: overzichtfile.write(overzicht)

# ----------------------------------------------------------------------
# ----- ME_DIALOG ------------------------------------------------------

class ME_19115_131(QtGui.QDialog):
  def __init__(self, parent):
    QtGui.QDialog.__init__(self)
    # Set up the user interface from Designer.
    self.parent = parent
    #~ self.ui = Ui_WfsClient()
    self.ui = Ui_ME_19115_131()
    self.ui.setupUi(self)
    self.start_dir, self.start_file = os.path.split(os.path.abspath(__file__))
    # maak een object van de configuratie data
    cfg = Config(parent.start_dir+os.sep+self.start_file.split('.')[0]+'.cfg')
    # lees de verschillende lijst tags uit
    self.codelijsten = cfg.get("codelijsten")
    self.eigenlijsten = cfg.get("eigenlijsten")
    
    # Identificatie algemeen
    # zet diverse tooltips
    self.ui.le_TitelBron.setToolTip(self.eigenlijsten["ToolTips"]["le_TitelBron"].decode('utf-8'))
    self.ui.le_AlternatieveTitel.setToolTip(self.eigenlijsten["ToolTips"]["le_AlternatieveTitel"].decode('utf-8'))
    self.ui.le_Versie.setToolTip(self.eigenlijsten["ToolTips"]["le_Versie"].decode('utf-8'))
    self.ui.le_uuid.setToolTip(self.eigenlijsten["ToolTips"]["le_uuid"].decode('utf-8'))
    self.ui.te_Samenvatting.setToolTip(self.eigenlijsten["ToolTips"]["te_Samenvatting"].decode('utf-8'))
    self.ui.le_Serienaamnummer.setToolTip(self.eigenlijsten["ToolTips"]["le_Serienaamnummer"].decode('utf-8'))
    self.ui.de_Creatiedatum.setToolTip(self.codelijsten["B.5.2 CI_DateTypeCode"][0][2].decode('utf-8'))
    self.ui.de_Publicatiedatum.setToolTip(self.codelijsten["B.5.2 CI_DateTypeCode"][1][2].decode('utf-8'))   
    self.ui.de_Revisiedatum.setToolTip(self.codelijsten["B.5.2 CI_DateTypeCode"][2][2].decode('utf-8'))  
    # PushButton "pb_uuid" voor een nieuwe uuid: zet een tooltip en als er op de button geklikt wordt
    self.ui.pb_uuid.setToolTip(self.eigenlijsten["ToolTips"]["pb_uuid"])
    self.ui.pb_uuid.clicked.connect(self.verander_uuid)
    # Combobox "cbx_Status" 
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de codelijst B.5.23 MD_ProgressCode en bepaal het item waarmee gestart wordt
    addItemsLijst = [self.codelijsten["B.5.23 MD_ProgressCode"][index][0] for index in range(len(self.codelijsten["B.5.23 MD_ProgressCode"]))]
    self.ui.cbx_Status.setToolTip(self.eigenlijsten["ToolTips"]["cbx_Status"])
    self.ui.cbx_Status.addItems(addItemsLijst)
    self.ui.cbx_Status.setCurrentIndex(addItemsLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_Status"]))
    # Combobox "cbx_Ruimtelijke_schema" 
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de codelijst B.5.26 MD_SpatialRepresentationTypeCode en bepaal het item waarmee gestart wordt
    addItemLijst = [self.codelijsten["B.5.26 MD_SpatialRepresentationTypeCode"][index][0] for index in range(len(self.codelijsten["B.5.26 MD_SpatialRepresentationTypeCode"]))]
    self.ui.cbx_Ruimtelijke_schema.setToolTip(self.eigenlijsten["ToolTips"]["cbx_Ruimtelijke_schema"])
    self.ui.cbx_Ruimtelijke_schema.addItems(addItemLijst)
    self.ui.cbx_Ruimtelijke_schema.setCurrentIndex(addItemLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_Ruimtelijke_schema"]))
    # Combobox "cbx_Taal_bron"
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de codelijst ISO 639-2 en bepaal het item waarmee gestart wordt
    addItemLijst = sorted([self.codelijsten["ISO 639-2"][index][0] for index in range(len(self.codelijsten["ISO 639-2"]))])
    self.ui.cbx_Taal_bron.setToolTip(self.eigenlijsten["ToolTips"]["cbx_Taal_bron"])
    self.ui.cbx_Taal_bron.addItems(addItemLijst)
    self.ui.cbx_Taal_bron.setCurrentIndex(addItemLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_Taal_bron"]))
    # Combobox "cbx_Karakterset_bron"
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de codelijst B.5.10 MD_CharacterSetCode en bepaal het item waarmee gestart wordt
    addItemLijst = [self.codelijsten["B.5.10 MD_CharacterSetCode"][index][0] for index in range(len(self.codelijsten["B.5.10 MD_CharacterSetCode"]))]
    self.ui.cbx_Karakterset_bron.setToolTip(self.eigenlijsten["ToolTips"]["cbx_Karakterset_bron"])
    self.ui.cbx_Karakterset_bron.addItems(addItemLijst)
    self.ui.cbx_Karakterset_bron.setCurrentIndex(addItemLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_Karakterset_bron"]))

    # Identificatie thema's
    # plaats Checkboxen "cb_Onderwerpen_*" in QScrollArea "sa_Onderwerpen"
    # vul de checkboxen met codelijst B.5.27 MD_TopicCategoryCode en lees de tooltips uit B.5.27 MD_TopicCategoryCode
    chkBoxLayout = QtGui.QVBoxLayout(self)
    for num in range(len(self.codelijsten["B.5.27 MD_TopicCategoryCode"])):
      exec("cb_Onderwerpen_%s = QtGui.QCheckBox(self)" %(num))
      exec("cb_Onderwerpen_%s.setText(self.codelijsten['B.5.27 MD_TopicCategoryCode'][%s][0].decode('utf-8'))" %(num, num))
      exec("cb_Onderwerpen_%s.setToolTip(self.codelijsten['B.5.27 MD_TopicCategoryCode'][%s][2])" %(num, num))
      exec("chkBoxLayout.addWidget(cb_Onderwerpen_%s)" %(num))
    chkBoxLayout.addStretch(1) 
    chkBoxLayout.setMargin(0);
    chkBoxLayout.setContentsMargins(30,15,10,10)
    chkBoxLayout.setSpacing(0) 
    widget = QtGui.QWidget()  
    widget.setLayout(chkBoxLayout)
    self.ui.sa_Onderwerpen.setWidget(widget)
    # Combobox "cbx_Thesaurus"
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de eigenlijst Voorkeuren
    addItemsLijst = self.eigenlijsten["Thesaurussen"].keys()
    self.ui.cbx_Thesaurus.setToolTip(self.eigenlijsten["ToolTips"]["cbx_Thesaurus"])
    self.ui.cbx_Thesaurus.addItems(addItemsLijst)
    self.ui.cbx_Thesaurus.setCurrentIndex(addItemsLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_Thesaurus"]))
    self.ui.cbx_Thesaurus.currentIndexChanged.connect(self.ThesaurusChanged)  
    # plaats Checkboxen "cb_Trefwoorden_*" in QScrollArea "sa_Trefwoorden"  
    # vul de checkboxen met eigenlijst Thesaurussen en lees de tooltips uit eigenlijst Thesaurussen
    actieve_thesaurus = self.ui.cbx_Thesaurus.currentText()
    addItemLijst = self.eigenlijsten["Thesaurussen"][actieve_thesaurus][1]
    chkBoxLayout = QtGui.QVBoxLayout(self)
    for num in range(len(addItemLijst)):
      exec("cb_Trefwoorden_%s = QtGui.QCheckBox(self)" %(num))
      exec("cb_Trefwoorden_%s.setText(addItemLijst[%s].decode('utf-8'))" %(num, num))
      exec("chkBoxLayout.addWidget(cb_Trefwoorden_%s)" %(num))
    chkBoxLayout.addStretch(1) 
    chkBoxLayout.setMargin(0);
    chkBoxLayout.setContentsMargins(30,15,10,10)
    chkBoxLayout.setSpacing(0) 
    widget = QtGui.QWidget()  
    widget.setLayout(chkBoxLayout)
    self.ui.sa_Trefwoorden.setWidget(widget)    

    # Identificatie beperkingen
    # zet diverse tooltips
    self.ui.lbl_Gebruiksbeperkingen.setToolTip(self.eigenlijsten["ToolTips"]["lbl_Gebruiksbeperkingen"])
    self.ui.lbl_Restricties.setToolTip(self.eigenlijsten["ToolTips"]["lbl_Restricties"].decode('utf-8'))
    self.ui.lbl_toegangsrestricties.setToolTip(self.eigenlijsten["ToolTips"]["lbl_toegangsrestricties"])
    self.ui.lbl_gebruiksrestricties.setToolTip(self.eigenlijsten["ToolTips"]["lbl_gebruiksrestricties"])    
    self.ui.lbl_Veiligheidsrestricties.setToolTip(self.eigenlijsten["ToolTips"]["lbl_Veiligheidsrestricties"])
    self.ui.lbl_Overige_beperkingen.setToolTip(self.eigenlijsten["ToolTips"]["lbl_Overige_beperkingen"])
    self.ui.lbl_Toelichting_veiligheidsrestricties.setToolTip(self.eigenlijsten["ToolTips"]["lbl_Toelichting_veiligheidsrestricties"])
    self.ui.le_veiligheidsrestricties.setToolTip(self.eigenlijsten["ToolTips"]["le_veiligheidsrestricties"])
    # vul de checkboxen met
    addItemLijst = self.eigenlijsten["Gebruiksbeperkingen"]
    chkBoxLayout = QtGui.QVBoxLayout(self)
    for num in range(len(addItemLijst)):
      exec("cb_Gebruiksbeperkingen_%s = QtGui.QCheckBox(self)" %(num))
      exec("cb_Gebruiksbeperkingen_%s.setText(addItemLijst[%s][0].decode('utf-8'))" %(num, num))
      exec("chkBoxLayout.addWidget(cb_Gebruiksbeperkingen_%s)" %(num))
    chkBoxLayout.addStretch(1) 
    chkBoxLayout.setMargin(0);
    chkBoxLayout.setContentsMargins(10,10,10,10)
    chkBoxLayout.setSpacing(0) 
    widget = QtGui.QWidget()  
    widget.setLayout(chkBoxLayout)
    self.ui.sa_Gebruiksbeperkingen.setWidget(widget)
    # plaats Checkboxen "cb_Gebruiksrestricties_*" in QScrollArea "sa_Gebruiksrestricties"
    # vul de checkboxen met codelijst B.5.24 MD_RestrictionCode en lees de tooltips uit B.5.24 MD_RestrictionCode
    chkBoxLayout = QtGui.QVBoxLayout(self)
    for num in range(len(self.codelijsten["B.5.24 MD_RestrictionCode"])):
      exec("cb_Gebruiksrestricties_%s = QtGui.QCheckBox(self)" %(num))
      exec("cb_Gebruiksrestricties_%s.setText(self.codelijsten['B.5.24 MD_RestrictionCode'][%s][0].decode('utf-8'))" %(num, num))
      exec("cb_Gebruiksrestricties_%s.setToolTip(self.codelijsten['B.5.24 MD_RestrictionCode'][%s][2])" %(num, num))
      exec("chkBoxLayout.addWidget(cb_Gebruiksrestricties_%s)" %(num))
    chkBoxLayout.addStretch(1) 
    chkBoxLayout.setMargin(0);
    chkBoxLayout.setContentsMargins(10,10,10,10)
    chkBoxLayout.setSpacing(0) 
    widget = QtGui.QWidget()  
    widget.setLayout(chkBoxLayout)
    self.ui.sa_Gebruiksrestricties.setWidget(widget)
    # plaats Checkboxen "cb_Toegangsrestricties_*" in QScrollArea "sa_Toegangsrestricties"
    # vul de checkboxen met codelijst B.5.24 MD_RestrictionCode en lees de tooltips uit B.5.24 MD_RestrictionCode
    chkBoxLayout = QtGui.QVBoxLayout(self)
    self.bg_Toegangsrestricties = QtGui.QButtonGroup()
    for num in range(len(self.codelijsten["B.5.24 MD_RestrictionCode"])):
      exec("self.cb_Toegangsrestricties_%s = QtGui.QCheckBox(self)" %(num))
      exec("self.cb_Toegangsrestricties_%s.setText(self.codelijsten['B.5.24 MD_RestrictionCode'][%s][0].decode('utf-8'))" %(num, num))
      exec("self.cb_Toegangsrestricties_%s.setToolTip(self.codelijsten['B.5.24 MD_RestrictionCode'][%s][2])" %(num, num))
      exec("self.bg_Toegangsrestricties.addButton(self.cb_Toegangsrestricties_%s, %s)" %(num, num+1))
      exec("chkBoxLayout.addWidget(self.cb_Toegangsrestricties_%s)" %(num))
    self.bg_Toegangsrestricties.buttonClicked[QtGui.QAbstractButton].connect(self.changed_Toegangsrestricties)
    self.bg_Toegangsrestricties.setExclusive(False)    
    chkBoxLayout.addStretch(1) 
    chkBoxLayout.setMargin(0);
    chkBoxLayout.setContentsMargins(10,10,10,10)
    chkBoxLayout.setSpacing(0) 
    widget = QtGui.QWidget()  
    widget.setLayout(chkBoxLayout)
    self.ui.sa_Toegangsrestricties.setWidget(widget)
    # plaats Checkboxen "cb_veiligheidsrestricties_*" in QScrollArea "sa_veiligheidsrestricties"
    # vul de checkboxen met codelijst B.5.11 MD_ClassificationCode en lees de tooltips uit B.5.11 MD_ClassificationCode
    chkBoxLayout = QtGui.QVBoxLayout(self)
    self.bg_veiligheidsrestricties = QtGui.QButtonGroup()
    for num in range(len(self.codelijsten["B.5.11 MD_ClassificationCode"])):
      exec("cb_veiligheidsrestricties_%s = QtGui.QCheckBox(self)" %(num))
      exec("cb_veiligheidsrestricties_%s.setText(self.codelijsten['B.5.11 MD_ClassificationCode'][%s][0].decode('utf-8'))" %(num, num))
      exec("cb_veiligheidsrestricties_%s.setToolTip(self.codelijsten['B.5.11 MD_ClassificationCode'][%s][2])" %(num, num))
      exec("self.bg_veiligheidsrestricties.addButton(cb_veiligheidsrestricties_%s, %s)" %(num, num+1))
      exec("chkBoxLayout.addWidget(cb_veiligheidsrestricties_%s)" %(num))
    #~ self.bg_veiligheidsrestricties.buttonClicked[QtGui.QAbstractButton].connect(self.changed_veiligheidsrestricties)
    chkBoxLayout.addStretch(1) 
    chkBoxLayout.setMargin(0);
    chkBoxLayout.setContentsMargins(10,10,10,10)
    chkBoxLayout.setSpacing(0) 
    widget = QtGui.QWidget()  
    widget.setLayout(chkBoxLayout)
    self.ui.sa_veiligheidsrestricties.setWidget(widget)        
    # plaats Checkboxen "cb_Overige_beperkingen_*" in QScrollArea "sa_Overige_beperkingen"
    # vul de checkboxen met
    self.bg_Overige_beperkingen = QtGui.QButtonGroup()
    chkBoxLayout = QtGui.QVBoxLayout(self)
    addItemLijst = self.eigenlijsten["Overige beperkingen"]
    for num in range(len(addItemLijst)):
      exec("self.cb_Overige_beperkingen_%s = QtGui.QCheckBox(self)" %(num))
      exec("self.cb_Overige_beperkingen_%s.setText(addItemLijst[%s][0].decode('utf-8')+'     '+addItemLijst[%s][1].decode('utf-8'))" %(num, num, num))
      exec("self.cb_Overige_beperkingen_%s.setToolTip(addItemLijst[%s][2].decode('utf-8'))" %(num, num))
      exec("self.bg_Overige_beperkingen.addButton(self.cb_Overige_beperkingen_%s, %s)" %(num, num+1))
      exec("chkBoxLayout.addWidget(self.cb_Overige_beperkingen_%s)" %(num))
    self.bg_Overige_beperkingen.buttonClicked[QtGui.QAbstractButton].connect(self.changed_Overige_beperkingen)
    self.bg_Overige_beperkingen.setExclusive(False)
    chkBoxLayout.addStretch(1) 
    chkBoxLayout.setMargin(0);
    chkBoxLayout.setContentsMargins(10,10,10,10)
    chkBoxLayout.setSpacing(0) 
    widget = QtGui.QWidget()  
    widget.setLayout(chkBoxLayout)
    self.ui.sa_Overige_beperkingen.setWidget(widget)
    
    # Identificatie beheer
    # zet diverse tooltips
    self.ui.te_Doel_vervaardiging.setToolTip(self.eigenlijsten["ToolTips"]["te_Doel_vervaardiging"].decode('utf-8'))
    self.ui.le_Toepassingsschaal_01.setToolTip(self.eigenlijsten["ToolTips"]["le_Toepassingsschaal"].decode('utf-8'))
    self.ui.le_Toepassingsschaal_02.setToolTip(self.eigenlijsten["ToolTips"]["le_Toepassingsschaal"].decode('utf-8'))    
    self.ui.le_Resolutie_01.setToolTip(self.eigenlijsten["ToolTips"]["le_Resolutie"].decode('utf-8'))
    self.ui.le_Resolutie_02.setToolTip(self.eigenlijsten["ToolTips"]["le_Resolutie"].decode('utf-8'))    
    self.ui.le_Aanvullende_informatie.setToolTip(self.eigenlijsten["ToolTips"]["le_Aanvullende_informatie"].decode('utf-8'))
    # Combobox "cbx_Herzieningsfrequentie"    
    # vul combobox cbx_Herzieningsfrequentie met de codelijst B.5.18 MD_MaintenanceFrequencyCode
    self.ui.cbx_Herzieningsfrequentie.addItems([self.codelijsten["B.5.18 MD_MaintenanceFrequencyCode"][index][0] for index in range(len(self.codelijsten["B.5.18 MD_MaintenanceFrequencyCode"]))])
    self.ui.cbx_Herzieningsfrequentie.setCurrentIndex(5)
    self.ui.cbx_Herzieningsfrequentie.setToolTip(self.eigenlijsten['ToolTips']['cbx_Herzieningsfrequentie'].decode('utf-8'))
    # LineEdit "le_gerelateerde_dataset"
    # zet de create datum van de eerste gerelateerde datasets op vandaag; als op enter gedrukt wordt start dan self.extra_gerelateerde_dataset
    # de overige lineedits en datums op hidden
    self.ui.le_gerelateerde_dataset_1.returnPressed.connect(self.extra_gerelateerde_dataset)
    self.ui.le_gerelateerde_dataset_1.setToolTip(self.eigenlijsten['ToolTips']['le_gerelateerde_dataset'].decode('utf-8'))
    self.ui.de_gd_Datum_1.setDate(QtCore.QDate.currentDate())
    self.ui.de_gd_Datum_1.setToolTip(self.eigenlijsten['ToolTips']['de_gd_Creatiedatum'].decode('utf-8'))
    self.ui.de_gd_Datum_1.setHidden(True)
    # zet labels uit
    self.ui.lbl_gd_Datumtype.setHidden(True)
    self.ui.lbl_gd_Datum.setHidden(True)
    # Combobox "cbx_gd_Datumtype_1" 
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de codelijst B.5.2 CI_DateTypeCode en bepaal het item waarmee gestart wordt
    addItemsLijst = [self.codelijsten["B.5.2 CI_DateTypeCode"][index][0] for index in range(len(self.codelijsten["B.5.2 CI_DateTypeCode"]))]
    self.ui.cbx_gd_Datumtype_1.setToolTip(self.eigenlijsten["ToolTips"]["cbx_gd_Datumtype"])
    self.ui.cbx_gd_Datumtype_1.addItems(addItemsLijst)
    self.ui.cbx_gd_Datumtype_1.setCurrentIndex(addItemsLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_gd_Datumtype"]))
    self.ui.cbx_gd_Datumtype_1.setHidden(True)
    # Vul de overige objecten van de gerelateerde dataset en zet ze op hidden
    for num in range(2, 7):
      exec("self.ui.le_gerelateerde_dataset_%s.setHidden(True)" %(num))
      exec("self.ui.de_gd_Datum_%s.setHidden(True)" %(num))
      exec("self.ui.de_gd_Datum_%s.setDate(QtCore.QDate.currentDate())" %(num))
      exec("self.ui.cbx_gd_Datumtype_%s.addItems(addItemsLijst)" %(num))
      exec("self.ui.cbx_gd_Datumtype_%s.setCurrentIndex(addItemsLijst.index(self.eigenlijsten['Voorkeuren']['cbx_gd_Datumtype']))" %(num))
      exec("self.ui.cbx_gd_Datumtype_%s.setHidden(True)" %(num))
      exec("self.ui.le_gerelateerde_dataset_%s.returnPressed.connect(self.extra_gerelateerde_dataset)" %(num))

    # kwaliteit algemeen
    # zet de diverse tooltips
    self.ui.le_topologische_samenhang.setToolTip(self.eigenlijsten["ToolTips"]["le_topologische_samenhang"].decode('utf-8'))
    self.ui.te_beschrijving_herkomst.setToolTip(self.eigenlijsten["ToolTips"]["te_beschrijving_herkomst"].decode('utf-8'))
    self.ui.te_nauwkeurigheid.setToolTip(self.eigenlijsten["ToolTips"]["te_nauwkeurigheid"].decode('utf-8'))
    self.ui.te_Compleetheid.setToolTip(self.eigenlijsten["ToolTips"]["te_Compleetheid"].decode('utf-8'))
    # Combobox "cbx_kwaliteitsbeschrijving"
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de eigenlijst Voorkeuren
    addItemsLijst = [self.codelijsten["B.5.25 MD_ScopeCode"][index][0] for index in range(len(self.codelijsten["B.5.25 MD_ScopeCode"]))]
    self.ui.cbx_kwaliteitsbeschrijving.setToolTip(self.eigenlijsten["ToolTips"]["cbx_kwaliteitsbeschrijving"])
    self.ui.cbx_kwaliteitsbeschrijving.addItems(addItemsLijst)
    self.ui.cbx_kwaliteitsbeschrijving.setCurrentIndex(addItemsLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_kwaliteitsbeschrijving"]))    
    # Combobox "cbx_Conformiteitsindicatie"
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de eigenlijst Voorkeuren
    addItemsLijst = [self.eigenlijsten["cbx_Conformiteitsindicatie"][index][0] for index in range(len(self.eigenlijsten["cbx_Conformiteitsindicatie"]))]
    self.ui.cbx_Conformiteitsindicatie.setToolTip(self.eigenlijsten["ToolTips"]["cbx_Conformiteitsindicatie"])
    self.ui.cbx_Conformiteitsindicatie.addItems(addItemsLijst)
    self.ui.cbx_Conformiteitsindicatie.setCurrentIndex(addItemsLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_Conformiteitsindicatie"]))
    # Combobox "cbx_specificatie_datum"
    # vul de itemlijst van de combobox, zet een tooltip, vul de combobox met de eigenlijst Voorkeuren
    addItemsLijst = [self.codelijsten["B.5.2 CI_DateTypeCode"][index][0] for index in range(len(self.codelijsten["B.5.2 CI_DateTypeCode"]))]
    addItemsLijst.append("")
    self.ui.cbx_specificatie_datum.setToolTip(self.eigenlijsten["ToolTips"]["cbx_specificatie_datum"])
    self.ui.cbx_specificatie_datum.addItems(addItemsLijst)
    self.ui.cbx_specificatie_datum.setCurrentIndex(addItemsLijst.index(self.eigenlijsten["Voorkeuren"]["cbx_specificatie_datum"]))
    # vul dateEdit de_specificatie_datum met huidige datum
    self.ui.de_specificatie_datum.setDate(QtCore.QDate.currentDate())

    # kwaliteit bewerking
    # TextEdit "te_beschrijving_bewerking"
    # 
    self.ui.te_beschrijving_bewerking_1.setToolTip(self.eigenlijsten['ToolTips']['te_beschrijving_bewerking'].decode('utf-8'))
    self.ui.de_kwaliteit_bewerking_1.setToolTip(self.eigenlijsten['ToolTips']['de_kwaliteit_bewerking'].decode('utf-8'))
    self.ui.cbx_bewerking_organisatie_1.setToolTip(self.eigenlijsten['ToolTips']['cbx_bewerking_organisatie'].decode('utf-8'))
    self.ui.cbx_rol_organisatie_1.setToolTip(self.eigenlijsten['ToolTips']['cbx_rol_organisatie'].decode('utf-8'))
    addItemsLijst = [self.codelijsten["B.5.5 CI_RoleCode"][index][0] for index in range(len(self.codelijsten["B.5.5 CI_RoleCode"]))]
    for num in range(1, 9):
      exec("self.ui.de_kwaliteit_bewerking_%s.setDate(QtCore.QDate.currentDate())" %(num))
      exec("self.ui.cbx_bewerking_organisatie_%s.addItems(self.eigenlijsten['Organisaties'])" %(num))
      exec("self.ui.cbx_bewerking_organisatie_%s.setCurrentIndex(self.eigenlijsten['Organisaties'].index(self.eigenlijsten['Voorkeuren']['cbx_bewerking_organisatie']))" %(num))
      exec("self.ui.cbx_rol_organisatie_%s.addItems(addItemsLijst)" %(num))
      exec("self.ui.cbx_rol_organisatie_%s.setCurrentIndex(addItemsLijst.index(self.eigenlijsten['Voorkeuren']['cbx_rol_organisatie']))" %(num))
    
    # kwaliteit bron
    self.ui.te_beschrijving_bron_1.setToolTip(self.eigenlijsten['ToolTips']['te_beschrijving_bron'].decode('utf-8'))
    self.ui.cbx_methode_bron_1.setToolTip(self.eigenlijsten['ToolTips']['cbx_methode_bron'].decode('utf-8'))
    self.ui.de_datum_bron_1.setToolTip(self.eigenlijsten['ToolTips']['de_datum_bron'].decode('utf-8'))
    self.ui.cbx_organisatie_bron_1.setToolTip(self.eigenlijsten['ToolTips']['cbx_organisatie_bron'].decode('utf-8'))
    self.ui.cbx_rol_bron_organisatie_1.setToolTip(self.eigenlijsten['ToolTips']['cbx_rol_bron_organisatie'].decode('utf-8'))    
    addItemsLijst = [self.codelijsten["B.5.5 CI_RoleCode"][index][0] for index in range(len(self.codelijsten["B.5.5 CI_RoleCode"]))]
    for num in range(1, 9):
      exec("self.ui.de_datum_bron_%s.setDate(QtCore.QDate.currentDate())" %(num))
      exec("self.ui.cbx_methode_bron_%s.addItems(self.eigenlijsten['Inwinningsmethode'])" %(num))
      exec("self.ui.cbx_organisatie_bron_%s.addItems(self.eigenlijsten['Organisaties'])" %(num))
      exec("self.ui.cbx_organisatie_bron_%s.setCurrentIndex(self.eigenlijsten['Organisaties'].index(self.eigenlijsten['Voorkeuren']['cbx_organisatie_bron']))" %(num))
      exec("self.ui.cbx_rol_bron_organisatie_%s.addItems(addItemsLijst)" %(num))
      exec("self.ui.cbx_rol_bron_organisatie_%s.setCurrentIndex(addItemsLijst.index(self.eigenlijsten['Voorkeuren']['cbx_rol_bron_organisatie']))" %(num))    
    
    # Dekking
    # zet diverse tooltips
    self.ui.le_naam_geo_gebied.setToolTip(self.eigenlijsten["ToolTips"]["le_naam_geo_gebied"])
    self.ui.de_temp_dekking_van.setToolTip(self.eigenlijsten["ToolTips"]["de_temp_dekking_van"])
    self.ui.de_temp_dekking_tot.setToolTip(self.eigenlijsten["ToolTips"]["de_temp_dekking_tot"])
    self.ui.te_temporele_dekking.setToolTip(self.eigenlijsten["ToolTips"]["te_temporele_dekking"])
    self.ui.cbx_horizontaal_referentiesysteem.setToolTip(self.eigenlijsten["ToolTips"]["cbx_horizontaal_referentiesysteem"])
    self.ui.le_horizontaal_Verantwoordelijk_organisatie.setToolTip(self.eigenlijsten["ToolTips"]["le_horizontaal_Verantwoordelijk_organisatie"])
    self.ui.cbx_verticaal_referentiesysteem.setToolTip(self.eigenlijsten["ToolTips"]["cbx_verticaal_referentiesysteem"])
    self.ui.le_verticaal_Verantwoordelijk_organisatie.setToolTip(self.eigenlijsten["ToolTips"]["le_verticaal_Verantwoordelijk_organisatie"])
    # zet voorkeuren
    self.ui.le_naam_geo_gebied.setText(self.eigenlijsten["Voorkeuren"]["le_naam_geo_gebied"])
    # Combobox "cbx_horizontaal_referentiesysteem"
    # vul combobox cbx_horizontaal_referentiesysteem met de self.eigenlijsten EPSG
    addItemLijst = [self.eigenlijsten["EPSG"][index][1]+" ("+str(self.eigenlijsten["EPSG"][index][0])+")" for index in range(len(self.eigenlijsten["EPSG"]))]
    self.ui.cbx_horizontaal_referentiesysteem.addItems(addItemLijst)
    self.ui.cbx_horizontaal_referentiesysteem.setCurrentIndex(4)     
    self.ui.le_horizontaal_Verantwoordelijk_organisatie.setText("EPSG")
    # Combobox "cbx_verticaal_referentiesysteem"
    # vul combobox cbx_verticaal_referentiesysteem met de self.eigenlijsten EPSG
    addItemLijst.append("")
    self.ui.cbx_verticaal_referentiesysteem.addItems(addItemLijst)
    self.ui.cbx_verticaal_referentiesysteem.setCurrentIndex(-1)
    # zet de verticale dekkingen op hidden
    self.ui.lbl_verticale_dekking.setHidden(True)
    self.ui.fr_verticale_dekking.setHidden(True)
    self.ui.lbl_max_Z.setHidden(True)
    self.ui.le_max_Z.setHidden(True)
    self.ui.lbl_min_Z.setHidden(True)
    self.ui.le_min_Z.setHidden(True)    
    # als cbx_verticaal_referentiesysteem wordt gewijzigd voer dan item_verticaal_referentiesysteem uit
    self.ui.cbx_verticaal_referentiesysteem.activated.connect(self.item_verticaal_referentiesysteem)
    # PushButton "pb_Voorbeeld"
    # verander het pad naar de image folder voor de pushbutton icon, zet een tooltip, zet het icon en als er op de button geklikt wordt    
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(_fromUtf8(parent.start_dir+"/images/folder.svg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.pb_Voorbeeld.setToolTip(self.eigenlijsten["ToolTips"]["pb_Voorbeeld"])
    self.ui.pb_Voorbeeld.setIcon(icon)
    self.ui.pb_Voorbeeld.clicked.connect(self.zoek_voorbeeld)
    # LineEdit "le_Voorbeeld"
    # plaats een tooltip en voer als op enter gedrukt wordt vul_image uit
    self.ui.le_Voorbeeld.setToolTip(self.eigenlijsten["ToolTips"]["le_Voorbeeld"])
    self.ui.le_Voorbeeld.returnPressed.connect(self.vul_image)
    
    # Inhoud
    # verander het pad naar de image folder voor de pushbutton icons
    #~ icon = QtGui.QIcon()
    #~ icon.addPixmap(QtGui.QPixmap(_fromUtf8(parent.start_dir+"/images/go-previous.svg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    #~ self.ui.clb_Object_informatie_vorige.setIcon(icon)
    #~ self.ui.clb_Atribuut_informatie_vorige.setIcon(icon)
    #~ icon = QtGui.QIcon()
    #~ icon.addPixmap(QtGui.QPixmap(_fromUtf8(parent.start_dir+"/images/go-next.svg")), QtGui.QIcon.Normal, QtGui.QIcon.Off)    
    #~ self.ui.clb_Object_informatie_volgende.setIcon(icon)
    #~ self.ui.clb_Atribuur_informatie_volgende.setIcon(icon)    

    # Algemeen Metadata
    # als PushButton nieuwe uuid wordt ingedrukt
    self.ui.pb_Metadata_uuid.clicked.connect(self.verander_metadata_uuid)  
    # vul dateEdit de_Wijzigingsdatum met huidige datum
    self.ui.de_Wijzigingsdatum.setDate(QtCore.QDate.currentDate())      

    self.ui.tableWidget_3.setColumnWidth(0, 400)
    self.ui.tableWidget_3.setColumnWidth(1, 200)
    self.ui.tableWidget_3.setColumnWidth(2, 20)

  def ThesaurusChanged(self):
    QtGui.QMessageBox.warning(None, 'LET OP !!!', 'Thesaurus is veranderd')
    
  def zoek_voorbeeld(self):
    bestandsnaam = QtGui.QFileDialog.getOpenFileName(self, "Voorbeeld Image", "", filter="*.png *.jpg *.jpeg")
    if bestandsnaam: 
      self.ui.le_Voorbeeld.setText(bestandsnaam)
      self.vul_image()

  def vul_image(self):
    bestandsnaam = self.ui.le_Voorbeeld.text()    
    if bestandsnaam.startswith("http://") or bestandsnaam.startswith("https://"):
      response = requests.get(bestandsnaam, verify=False, timeout=50)
      im = Image.open(StringIO(response.content))
      bestandsnaam = "%s%s/URLimage.jpg" %(tempfile.gettempdir(), os.sep)
      im.save(bestandsnaam)
    breedte = self.ui.fr_dekking.frameGeometry().width()
    hoogte = self.ui.fr_dekking.frameGeometry().height()
    label = QtGui.QLabel()
    image = QtGui.QPixmap(bestandsnaam)
    scaledImage = image.scaled(breedte, hoogte, QtCore.Qt.KeepAspectRatio)
    label.setPixmap(scaledImage)
    label.setAlignment(QtCore.Qt.AlignCenter)
    #~ self.ui.fr_dekking.setLayout(self.layout())
    HBox = QtGui.QHBoxLayout()
    #~ HBox.itemAt(0).widget().setParent(None)
    #~ HBox.takeAt(0).setParent(None)
    #~ HBox.setParent(None)
    HBox.addWidget(label)
    #~ QtGui.QMessageBox.warning(None, 'LET OP !!!', '%s' %(HBox.count()))
    #~ HBox.itemAt(1).widget().setParent(None)
    #~ https://stackoverflow.com/questions/4528347/clear-all-widgets-in-a-layout-in-pyqt#13103617
    self.ui.fr_dekking.setLayout(HBox)
    self.ui.fr_dekking.update()
    
  def item_verticaal_referentiesysteem(self, index):
    if self.ui.cbx_verticaal_referentiesysteem.itemText(index)!= "":
      self.ui.le_verticaal_Verantwoordelijk_organisatie.setText("EPSG")
      self.ui.lbl_verticale_dekking.show()
      self.ui.fr_verticale_dekking.show()
      self.ui.lbl_max_Z.show()
      self.ui.le_max_Z.show()
      self.ui.lbl_min_Z.show()
      self.ui.le_min_Z.show()
    else:
      self.ui.le_verticaal_Verantwoordelijk_organisatie.setText("")
      self.ui.lbl_verticale_dekking.setHidden(True)
      self.ui.fr_verticale_dekking.setHidden(True)
      self.ui.lbl_max_Z.setHidden(True)
      self.ui.le_max_Z.setHidden(True)
      self.ui.lbl_min_Z.setHidden(True)
      self.ui.le_min_Z.setHidden(True)
      
  def verander_uuid(self):
    msgbox = QtGui.QMessageBox.warning(None, "LET OP !!!", "Weet u zeker dat u de UUID wil aanpassen?", QtGui.QMessageBox.Cancel|QtGui.QMessageBox.Ok, QtGui.QMessageBox.Cancel)
    if msgbox == QtGui.QMessageBox.Ok: self.ui.le_uuid.setText(str(uuid.uuid4()))

  def verander_metadata_uuid(self):
    msgbox = QtGui.QMessageBox.warning(None, "LET OP !!!", "Weet u zeker dat u de Metadata UUID wil aanpassen?", QtGui.QMessageBox.Cancel|QtGui.QMessageBox.Ok, QtGui.QMessageBox.Cancel)
    if msgbox == QtGui.QMessageBox.Ok: self.ui.le_Metadata_uuid.setText(str(uuid.uuid4()))

  def changed_Toegangsrestricties(self, cb_num):
    pass 
    
  def changed_Overige_beperkingen(self, cb_num):
    if cb_num.isChecked() == True:
      for num in range(len(self.codelijsten["B.5.24 MD_RestrictionCode"])):
        exec("if self.cb_Toegangsrestricties_%s.text() == 'Anders':\n  self.cb_Toegangsrestricties_%s.setChecked(True)" %(num, num))
      if cb_num.text().endswith("http://creativecommons.org/publicdomain/mark/1.0/deed.nl") or cb_num.text().endswith("http://creativecommons.org/publicdomain/zero/1.0/deed.nl"):
        for num in range(len(self.eigenlijsten["Overige beperkingen"])): 
          exec("if self.cb_Overige_beperkingen_%s.text().strip() == 'geen beperkingen':\n  self.cb_Overige_beperkingen_%s.setChecked(True)"  %(num, num))

  def extra_gerelateerde_dataset(self):
    for num in range(2, 7): 
      #~ self.ui.de_gd_Datum_1.setHidden(False); \
      #~ self.ui.cbx_gd_Datumtype_1.setHidden(False); \
      exec_tekst = """if self.ui.le_gerelateerde_dataset_%s.text() != '': \
      self.ui.lbl_gd_Datumtype.setHidden(False); \
      self.ui.lbl_gd_Datum.setHidden(False); \
      self.ui.le_gerelateerde_dataset_%s.setHidden(False); \
      self.ui.cbx_gd_Datumtype_%s.setHidden(False); \
      self.ui.de_gd_Datum_%s.setHidden(False)"""%(num-1, num, num-1, num-1)
      exec(exec_tekst)
      exec_tekst = """if self.ui.le_gerelateerde_dataset_%s.text() != '' and num == 6: \
      self.ui.cbx_gd_Datumtype_%s.setHidden(False); \
      self.ui.de_gd_Datum_%s.setHidden(False)""" %(num, num, num)
      exec(exec_tekst)

  def extra_beschrijving_bewerking(self):
    QtGui.QMessageBox.warning(None, 'LET OP !!!', 'tekst is veranderd ')  
      
      
    #~ gd_num = int(self.sender().objectName().split('_')[-1])
    #~ if gd_num < 6: 
      #~ exec("self.ui.le_gerelateerde_dataset_%s.setHidden(False)" %(gd_num+1))
      #~ exec("self.ui.de_gd_Creatiedatum_%s.setHidden(False)" %(gd_num+1))
      #~ exec("self.ui.de_gd_Publicatiedatum_%s.setHidden(False)" %(gd_num+1))
      #~ exec("self.ui.de_gd_Revisiedatum_%s.setHidden(False)" %(gd_num+1))


    
    
          #~ self.ui.sa_Overige_beperkingen.update()
        #~ QtGui.QMessageBox.warning(None, 'LET OP !!!', str(cb_num.isChecked()+' '+cb_num.text()))
        
      #~ exec("cb_Overige_beperkingen_%s = QtGui.QCheckBox(self)" %(num))
      #~ exec("cb_Overige_beperkingen_%s.setText(addItemLijst[%s][0].decode('utf-8')+'     '+addItemLijst[%s][1].decode('utf-8'))" %(num, num, num))
      #~ exec("cb_Overige_beperkingen_%s.setToolTip(addItemLijst[%s][2].decode('utf-8'))" %(num, num))
      
        #~ exec("if self.cb_Toegangsrestricties_%s.text() == 'Anders':\n  self.cb_Toegangsrestricties_%s.setChecked(True)\nelse: self.cb_Toegangsrestricties_%s.setChecked(False)" %(num, num, num))
         
          
      
      #~ QtGui.QMessageBox.warning(None, "LET OP !!!", "Checkbox "+str(cb_num.isChecked())+" "+cb_num.text()+" is veranderd")
#~ 
  #~ def changed_veiligheidsrestricties(self):
    #~ pass
    #~ QtGui.QMessageBox.warning(None, "LET OP !!!", "Checkbox veiligheidsrestricties veranderd")

    #~ "http://maps.noord-holland.nl/WAF/Voorbeelden/Geschiktheid_Warmte_Koude_Opslag_diep.jpg"
    #~ "http://geoservices.provinciegroningen.nl/bestanden/metadata/voorbeeldplaatjes/LuchthavensLuchthavengebieden.jpg"
    #~ "http://services.geodata-utrecht.nl:80/geoserver/w01_3_oppervlaktewater/wms?request=GetMap&service=WMS&SRS=EPSG:28992&CRS=EPSG:28992&bbox=112656.06,436541.64,173834.94,482076.36&width=600&height=446&format=image/png&styles=&layers=Zwemwaterlocaties"
    #~ "/home/jan/Downloads/Toeristenkaart Sri Lanka.jpg"



    #~ QtGui.QMessageBox.warning(None, "LET OP !!!", str(addItemLijst))

      #~ QtGui.QMessageBox.warning(None, "",str(uuid.uuid4()))

    #~ QtGui.QMessageBox.information(None, "OnlineResource Error", parent.start_dir)


        #~ self.settings = QtCore.QSettings()
#~ 
        #~ logformat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        #~ logfile = self.get_temppath("wfs20client.log")
        #~ logging.basicConfig(filename=logfile, level=logging.DEBUG, format=logformat)
#~ 
        #~ self.logger = logging.getLogger('WFS 2.0 Client')
#~ 
        #~ self.ui.frmExtent.show()
        #~ self.ui.frmParameter.hide()
        #~ self.ui.progressBar.setVisible(False)
        #~ self.ui.cmdListStoredQueries.setVisible(False)
#~ 
        #~ # Load default onlineresource
        #~ if url:
            #~ self.ui.txtUrl.setText(url)
        #~ else:
            #~ self.ui.txtUrl.setText(self.get_url())
        #~ self.ui.txtCount.setText(self.get_featurelimit())
#~ 
        #~ self.ui.txtUsername.setVisible(False)
        #~ self.ui.txtPassword.setVisible(False)
        #~ self.ui.lblUsername.setVisible(False)
        #~ self.ui.lblPassword.setVisible(False)
#~ 
        #~ self.parameter_lineedits = []
        #~ self.parameter_labels = []
#~ 
        #~ self.init_variables()
#~ 
        #~ self.onlineresource = ""
        #~ self.vendorparameters = ""
#~ 
        #~ self.ui.lblMessage.setText("SRS is set to EPSG: {0}".format(str(self.parent.iface.mapCanvas().mapRenderer().destinationCrs().postgisSrid())))
        #~ self.ui.txtSrs.setText("urn:ogc:def:crs:EPSG::{0}".format(str(self.parent.iface.mapCanvas().mapRenderer().destinationCrs().postgisSrid())))
#~ 
        #~ QtCore.QObject.connect(self.ui.cmdGetCapabilities, QtCore.SIGNAL("clicked()"), self.getCapabilities)
        #~ QtCore.QObject.connect(self.ui.cmdListStoredQueries, QtCore.SIGNAL("clicked()"), self.listStoredQueries)
        #~ QtCore.QObject.connect(self.ui.cmdGetFeature, QtCore.SIGNAL("clicked()"), self.getFeature)
        #~ QtCore.QObject.connect(self.ui.cmdMetadata, QtCore.SIGNAL("clicked()"), self.show_metadata)
        #~ QtCore.QObject.connect(self.ui.cmdExtent, QtCore.SIGNAL("clicked()"), self.show_extent)
        #~ QtCore.QObject.connect(self.ui.chkExtent, QtCore.SIGNAL("clicked()"), self.update_extent_frame)
        #~ QtCore.QObject.connect(self.ui.chkAuthentication, QtCore.SIGNAL("clicked()"), self.update_authentication)
        #~ QtCore.QObject.connect(self.ui.cmbFeatureType, QtCore.SIGNAL("currentIndexChanged(int)"), self.update_ui)
        #~ QtCore.QObject.connect(self.ui.txtUrl, QtCore.SIGNAL("textChanged(QString)"), self.check_url)
        #~ self.check_url(self.ui.txtUrl.text().strip())
#~ 
        #~ if url:
            #~ self.getCapabilities()
#~ 
    #~ def init_variables(self):
        #~ self.columnid = 0
        #~ self.bbox = ""
        #~ self.querytype = ""
        #~ self.featuretypes = {}
        #~ self.storedqueries = {}
#~ 
    #~ # Process GetCapabilities-Request
    #~ def getCapabilities(self):
        #~ self.init_variables()
        #~ self.ui.cmdGetFeature.setEnabled(False);
        #~ self.ui.cmbFeatureType.clear()
        #~ self.ui.frmExtent.show()
        #~ self.ui.frmParameter.hide()
        #~ self.ui.chkExtent.setChecked(False)
        #~ self.ui.txtExtentWest.setText("")
        #~ self.ui.txtExtentEast.setText("")
        #~ self.ui.txtExtentNorth.setText("")
        #~ self.ui.txtExtentSouth.setText("")
        #~ self.ui.cmdMetadata.setVisible(True)
        #~ self.ui.cmdExtent.setVisible(True)
        #~ self.ui.lblCount.setVisible(True)
        #~ self.ui.txtCount.setText(self.get_featurelimit())
        #~ self.ui.txtCount.setVisible(True)
        #~ self.ui.lblSrs.setVisible(True)
        #~ self.ui.txtSrs.setText("urn:ogc:def:crs:EPSG::{0}".format(str(self.parent.iface.mapCanvas().mapRenderer().destinationCrs().postgisSrid())))
        #~ self.ui.txtSrs.setVisible(True)
        #~ self.ui.txtFeatureTypeTitle.setVisible(False)
        #~ self.ui.txtFeatureTypeDescription.setVisible(False)
        #~ self.ui.lblInfo.setText("FeatureTypes")
        #~ self.ui.lblMessage.setText("")
#~ 
        #~ try:
            #~ self.onlineresource = self.ui.txtUrl.text().strip()
            #~ if len(self.onlineresource) == 0:
                #~ QtGui.QMessageBox.critical(self, "OnlineResource Error", "Not a valid OnlineResource!")
                #~ return
            #~ if "?" in self.onlineresource:
                #~ request = "{0}{1}".format(self.onlineresource, self.fix_acceptversions(self.onlineresource, "&"))
            #~ else:
                #~ request = "{0}{1}".format(self.onlineresource, self.fix_acceptversions(self.onlineresource, "?"))
            #~ if self.ui.chkAuthentication.isChecked():
                #~ self.setup_urllib2(request, self.ui.txtUsername.text().strip(), self.ui.txtPassword.text().strip())
            #~ else:
                #~ self.setup_urllib2(request, "", "")
            #~ self.logMessage(request)
            #~ response = urllib2.urlopen(request, None, 10)
            #~ buf = response.read()
        #~ except urllib2.HTTPError, e:
            #~ QtGui.QMessageBox.critical(self, "HTTP Error", "HTTP Error: {0}".format(e.code))
            #~ if e.code == 401:
                #~ self.ui.chkAuthentication.setChecked(True)
                #~ self.update_authentication()
        #~ except urllib2.URLError, e:
            #~ QtGui.QMessageBox.critical(self, "URL Error", "URL Error: {0}".format(e.reason))
        #~ else:
            #~ # process Response
            #~ root = ElementTree.fromstring(buf)
            #~ if self.is_wfs20_capabilties(root):
                #~ # WFS 2.0 Namespace
                #~ nswfs = "{http://www.opengis.net/wfs/2.0}"
                #~ nsxlink = "{http://www.w3.org/1999/xlink}"
                #~ nsows = "{http://www.opengis.net/ows/1.1}"
                #~ # GetFeature OnlineResource
                #~ for target in root.findall("{0}OperationsMetadata/{0}Operation".format(nsows)):
                    #~ if target.get("name") == "GetFeature":
                        #~ for subtarget in target.findall("{0}DCP/{0}HTTP/{0}Get".format(nsows)):
                            #~ getfeatureurl = subtarget.get("{0}href".format(nsxlink))
                            #~ if not "?" in getfeatureurl:
                                #~ self.onlineresource = getfeatureurl
                            #~ else:
                                #~ self.onlineresource = getfeatureurl[:getfeatureurl.find("?")]
                                #~ self.vendorparameters = getfeatureurl[getfeatureurl.find("?"):].replace("?", "&")
                #~ for target in root.findall("{0}FeatureTypeList/{0}FeatureType".format(nswfs)):
                    #~ for name in target.findall("{0}Name".format(nswfs)):
                        #~ self.ui.cmbFeatureType.addItem(name.text,name.text)
                        #~ featuretype = wfs20lib.FeatureType(name.text)
                        #~ if ":" in name.text:
                            #~ nsmap = self.get_namespace_map(buf)
                            #~ for prefix in nsmap:
                                #~ if prefix == name.text[:name.text.find(":")]:
                                    #~ featuretype.setNamespace(nsmap[prefix])
                                    #~ featuretype.setNamespacePrefix(prefix)
                                    #~ break
                        #~ for title in target.findall("{0}Title".format(nswfs)):
                            #~ featuretype.setTitle(title.text)
                        #~ for abstract in target.findall("{0}Abstract".format(nswfs)):
                            #~ featuretype.setAbstract(abstract.text)
                        #~ for metadata_url in target.findall("{0}MetadataURL".format(nswfs)):
                            #~ featuretype.setMetadataUrl(metadata_url.get("{0}href".format(nsxlink)))
                        #~ for bbox in target.findall("{0}WGS84BoundingBox".format(nsows)):
                            #~ for lowercorner in bbox.findall("{0}LowerCorner".format(nsows)):
                                #~ featuretype.setWgs84BoundingBoxEast(lowercorner.text.split(' ')[0])
                                #~ featuretype.setWgs84BoundingBoxSouth(lowercorner.text.split(' ')[1])
                            #~ for uppercorner in bbox.findall("{0}UpperCorner".format(nsows)):
                                #~ featuretype.setWgs84BoundingBoxWest(uppercorner.text.split(' ')[0])
                                #~ featuretype.setWgs84BoundingBoxNorth(uppercorner.text.split(' ')[1])
                        #~ self.featuretypes[name.text] = featuretype
                        #~ self.querytype="adhocquery"
            #~ else:
                #~ self.ui.lblMessage.setText("")
            #~ self.update_ui()
#~ 
            #~ # Lock
            #~ self.ui.cmdGetCapabilities.setText("List FeatureTypes")
            #~ self.ui.cmdListStoredQueries.setVisible(True)
            #~ self.ui.chkAuthentication.setEnabled(False)
            #~ self.ui.txtUrl.setEnabled(False)
            #~ self.ui.txtUsername.setEnabled(False)
            #~ self.ui.txtPassword.setEnabled(False)
#~ 
#~ 
    #~ #Process ListStoredQueries-Request
    #~ def listStoredQueries(self):
        #~ self.init_variables()
        #~ self.ui.cmdGetFeature.setEnabled(False);
        #~ self.ui.cmbFeatureType.clear()
        #~ self.ui.frmExtent.hide()
        #~ self.ui.frmParameter.show()
        #~ self.layout_reset()
        #~ self.ui.cmdMetadata.setVisible(False)
        #~ self.ui.cmdExtent.setVisible(False)
        #~ self.ui.lblCount.setVisible(False)
        #~ self.ui.txtCount.setText("")
        #~ self.ui.txtCount.setVisible(False)
        #~ self.ui.lblSrs.setVisible(False)
        #~ self.ui.txtSrs.setVisible(False)
        #~ self.ui.txtFeatureTypeTitle.setVisible(False)
        #~ self.ui.txtFeatureTypeDescription.setVisible(False)
        #~ self.ui.lblInfo.setText("StoredQueries")
        #~ self.ui.lblMessage.setText("")
        #~ try:
            #~ # self.onlineresource = self.ui.txtUrl.text().trimmed()
            #~ if not self.onlineresource:
                #~ QtGui.QMessageBox.critical(self, "OnlineResource Error", "Not a valid OnlineResource!")
                #~ return
            #~ if "?" in self.onlineresource:
                #~ request = "{0}&service=WFS&version=2.0.0&request=DescribeStoredQueries".format(self.onlineresource)
            #~ else:
                #~ request = "{0}?service=WFS&version=2.0.0&request=DescribeStoredQueries".format(self.onlineresource)
            #~ request += self.vendorparameters
            #~ if self.ui.chkAuthentication.isChecked():
                #~ self.setup_urllib2(request, self.ui.txtUsername.text().strip(), self.ui.txtPassword.text().strip())
            #~ else:
                #~ self.setup_urllib2(request, "", "")
            #~ self.logMessage(request)
            #~ response = urllib2.urlopen(request, None, 10)
            #~ buf = response.read()
        #~ except urllib2.HTTPError, e:
            #~ QtGui.QMessageBox.critical(self, "HTTP Error", "HTTP Error: {0}".format(e.code))
            #~ if e.code == 401:
                #~ self.ui.chkAuthentication.setChecked(True)
                #~ self.update_authentication()
        #~ except urllib2.URLError, e:
            #~ QtGui.QMessageBox.critical(self, "URL Error", "URL Error: {0}".format(e.reason))
        #~ else:
            #~ # process Response
            #~ root = ElementTree.fromstring(buf)
            #~ # WFS 2.0 Namespace
            #~ namespace = "{http://www.opengis.net/wfs/2.0}"
            #~ # check correct Rootelement
            #~ if root.tag == "{0}DescribeStoredQueriesResponse".format(namespace):
                #~ for target in root.findall("{0}StoredQueryDescription".format(namespace)):
                    #~ self.ui.cmbFeatureType.addItem(target.get("id"),target.get("id"))
                    #~ lparameter = []
                    #~ for parameter in target.findall("{0}Parameter".format(namespace)):
                        #~ lparameter.append(wfs20lib.StoredQueryParameter(parameter.get("name"), parameter.get("type")))
                    #~ storedquery = wfs20lib.StoredQuery(target.get("id"), lparameter)
                    #~ for title in target.findall("{0}Title".format(namespace)):
                        #~ storedquery.setTitle(title.text)
                    #~ for abstract in target.findall("{0}Abstract".format(namespace)):
                        #~ storedquery.setAbstract(abstract.text)
                    #~ self.storedqueries[target.get("id")] = storedquery
                    #~ self.querytype="storedquery" #R
            #~ else:
                #~ QtGui.QMessageBox.critical(self, "Error", "Not a valid DescribeStoredQueries-Response!")
            #~ self.update_ui()
#~ 
#~ 
    #~ # Process GetFeature-Request
    #~ def getFeature(self):
        #~ self.ui.lblMessage.setText("Please wait while downloading!")
        #~ if self.querytype == "storedquery":
            #~ query_string = "?service=WFS&request=GetFeature&version=2.0.0&STOREDQUERY_ID={0}".format(self.ui.cmbFeatureType.currentText())
            #~ storedquery = self.storedqueries[self.ui.cmbFeatureType.currentText()]
            #~ lparameter = storedquery.getStoredQueryParameterList()
            #~ for i in range(len(lparameter)):
                #~ if not lparameter[i].isValidValue(self.parameter_lineedits[i].text().strip()):
                    #~ QtGui.QMessageBox.critical(self, "Validation Error", lparameter[i].getName() + ": Value validation failed!")
                    #~ self.ui.lblMessage.setText("")
                    #~ return
                #~ query_string+= "&{0}={1}".format(urllib.quote(lparameter[i].getName()),urllib.quote(self.parameter_lineedits[i].text().strip()))
        #~ else :
            #~ # FIX
            #~ featuretype = self.featuretypes[self.ui.cmbFeatureType.currentText()]
            #~ typeNames=urllib.quote(self.ui.cmbFeatureType.currentText().encode('utf8'))
            #~ if len(self.bbox) < 1:
                #~ query_string = "?service=WFS&request=GetFeature&version=2.0.0&srsName={0}&typeNames={1}".format(self.ui.txtSrs.text().strip(), typeNames)
            #~ else:
                #~ query_string = "?service=WFS&request=GetFeature&version=2.0.0&srsName={0}&typeNames={1}&bbox={2}".format(self.ui.txtSrs.text().strip(), typeNames, self.bbox)
#~ 
            #~ if len(featuretype.getNamespace()) > 0 and len(featuretype.getNamespacePrefix()) > 0:
                #~ #query_string += "&namespace=xmlns({0}={1})".format(featuretype.getNamespacePrefix(), urllib.quote(featuretype.getNamespace(),""))
                #~ query_string += "&namespaces=xmlns({0},{1})".format(featuretype.getNamespacePrefix(), urllib.quote(featuretype.getNamespace(),""))
#~ 
            #~ if len(self.ui.txtCount.text().strip()) > 0:
                #~ query_string+= "&count={0}".format(self.ui.txtCount.text().strip())
            #~ # /FIX
#~ 
        #~ query_string+=self.vendorparameters
#~ 
        #~ resolvedepth = self.settings.value("/Wfs20Client/resolveDepth")
        #~ if resolvedepth:
            #~ query_string+="&resolve=local&resolvedepth={0}".format(resolvedepth)
#~ 
        #~ self.logMessage(self.onlineresource + query_string)
#~ 
        #~ self.httpGetId = 0
        #~ self.httpRequestAborted = False
#~ 
        #~ self.setup_qhttp()
        #~ self.http.requestFinished.connect(self.httpRequestFinished)
        #~ self.http.dataReadProgress.connect(self.updateDataReadProgress)
        #~ self.http.responseHeaderReceived.connect(self.readResponseHeader)
        #~ self.http.authenticationRequired.connect(self.authenticationRequired)
        #~ self.http.sslErrors.connect(self.sslErrors)
#~ 
        #~ layername="wfs{0}".format(''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6)))
        #~ self.downloadFile(self.onlineresource, query_string, self.get_temppath("{0}.gml".format(layername)))
#~ 
#~ 
    #~ """
    #~ ############################################################################################################################
    #~ # UI
    #~ ############################################################################################################################
    #~ """
#~ 
#~ 
    #~ # UI: Update SSL-Warning
    #~ def check_url(self, url):
        #~ if (url.startswith("https")):
            #~ self.ui.lblWarning.setVisible(True)
        #~ else:
            #~ self.ui.lblWarning.setVisible(False)
#~ 
    #~ # UI: Update Parameter-Frame
    #~ def update_ui(self):
#~ 
        #~ if self.querytype == "adhocquery":
            #~ featuretype = self.featuretypes[self.ui.cmbFeatureType.currentText()]
#~ 
            #~ if featuretype.getTitle():
                #~ if len(featuretype.getTitle()) > 0:
                    #~ self.ui.txtFeatureTypeTitle.setVisible(True)
                    #~ self.ui.txtFeatureTypeTitle.setPlainText(featuretype.getTitle())
                #~ else:
                    #~ self.ui.txtFeatureTypeTitle.setVisible(False)
            #~ else:
                #~ self.ui.txtFeatureTypeTitle.setVisible(False)
#~ 
            #~ if featuretype.getAbstract():
                #~ if len(featuretype.getAbstract()) > 0:
                    #~ self.ui.txtFeatureTypeDescription.setVisible(True)
                    #~ self.ui.txtFeatureTypeDescription.setPlainText(featuretype.getAbstract())
                #~ else:
                    #~ self.ui.txtFeatureTypeDescription.setVisible(False)
            #~ else:
                #~ self.ui.txtFeatureTypeDescription.setVisible(False)
#~ 
            #~ self.show_metadata_button(True)
            #~ self.show_extent_button(True)
            #~ self.ui.cmdGetFeature.setEnabled(True)
            #~ self.ui.lblMessage.setText("")
#~ 
        #~ if self.querytype == "storedquery":
            #~ storedquery = self.storedqueries[self.ui.cmbFeatureType.currentText()]
#~ 
            #~ if storedquery.getTitle():
                #~ if len(storedquery.getTitle()) > 0:
                    #~ self.ui.txtFeatureTypeTitle.setVisible(True)
                    #~ self.ui.txtFeatureTypeTitle.setPlainText(storedquery.getTitle())
                #~ else:
                    #~ self.ui.txtFeatureTypeTitle.setVisible(False)
            #~ else:
                #~ self.ui.txtFeatureTypeTitle.setVisible(False)
            #~ if storedquery.getAbstract():
                #~ if len(storedquery.getAbstract()) > 0:
                    #~ self.ui.txtFeatureTypeDescription.setVisible(True)
                    #~ self.ui.txtFeatureTypeDescription.setPlainText(storedquery.getAbstract())
                #~ else:
                    #~ self.ui.txtFeatureTypeDescription.setVisible(False)
            #~ else:
                #~ self.ui.txtFeatureTypeDescription.setVisible(False)
#~ 
            #~ self.ui.cmdGetFeature.setEnabled(True)
            #~ self.ui.lblMessage.setText("")
            #~ self.layout_reset()
            #~ for parameter in storedquery.getStoredQueryParameterList():
                #~ self.layout_add_parameter(parameter)
#~ 
#~ 
    #~ # UI: Update Extent-Frame
    #~ def update_extent_frame(self):
        #~ if self.ui.chkExtent.isChecked():
            #~ canvas=self.parent.iface.mapCanvas()
            #~ ext=canvas.extent()
            #~ self.ui.txtExtentWest.setText('%s'%ext.xMinimum())
            #~ self.ui.txtExtentEast.setText('%s'%ext.xMaximum())
            #~ self.ui.txtExtentNorth.setText('%s'%ext.yMaximum())
            #~ self.ui.txtExtentSouth.setText('%s'%ext.yMinimum())
#~ 
            #~ if (epsglib.isAxisOrderLatLon(self.ui.txtSrs.text().strip())):
                #~ self.bbox='%s'%ext.yMinimum() + "," + '%s'%ext.xMinimum() + "," + '%s'%ext.yMaximum() + "," + '%s'%ext.xMaximum() + ",{0}".format(self.ui.txtSrs.text().strip())
            #~ else:
                #~ self.bbox='%s'%ext.xMinimum() + "," + '%s'%ext.yMinimum() + "," + '%s'%ext.xMaximum() + "," + '%s'%ext.yMaximum() + ",{0}".format(self.ui.txtSrs.text().strip())
        #~ else:
            #~ self.ui.txtExtentWest.setText("")
            #~ self.ui.txtExtentEast.setText("")
            #~ self.ui.txtExtentNorth.setText("")
            #~ self.ui.txtExtentSouth.setText("")
            #~ self.bbox=""
#~ 
    #~ # UI: Update Main-Frame / Enable|Disable Authentication
    #~ def update_authentication(self):
        #~ if not self.ui.chkAuthentication.isChecked():
            #~ self.ui.frmMain.setGeometry(QtCore.QRect(10,90,501,551))
            #~ self.ui.txtUsername.setVisible(False)
            #~ self.ui.txtPassword.setVisible(False)
            #~ self.ui.lblUsername.setVisible(False)
            #~ self.ui.lblPassword.setVisible(False)
            #~ self.resize(516, 648)
        #~ else:
            #~ self.ui.frmMain.setGeometry(QtCore.QRect(10,150,501,551))
            #~ self.ui.txtUsername.setVisible(True)
            #~ self.ui.txtPassword.setVisible(True)
            #~ self.ui.lblUsername.setVisible(True)
            #~ self.ui.lblPassword.setVisible(True)
            #~ self.resize(516, 704)
#~ 
#~ 
    #~ # GridLayout reset (StoredQueries)
    #~ def layout_reset(self):
        #~ for qlabel in self.parameter_labels:
            #~ self.ui.gridLayout.removeWidget(qlabel)
            #~ qlabel.setParent(None) # http://www.riverbankcomputing.com/pipermail/pyqt/2008-March/018803.html
#~ 
        #~ for qlineedit in self.parameter_lineedits:
            #~ self.ui.gridLayout.removeWidget(qlineedit)
            #~ qlineedit.setParent(None) # http://www.riverbankcomputing.com/pipermail/pyqt/2008-March/018803.html
#~ 
        #~ del self.parameter_labels[:]
        #~ del self.parameter_lineedits[:]
        #~ self.columnid = 0
#~ 
#~ 
    #~ # GridLayout addParameter (StoredQueries)
    #~ def layout_add_parameter(self, storedqueryparameter):
        #~ qlineedit = QtGui.QLineEdit()
        #~ qlabelname = QtGui.QLabel()
        #~ qlabelname.setText(storedqueryparameter.getName())
        #~ qlabeltype = QtGui.QLabel()
        #~ qlabeltype.setText(storedqueryparameter.getType().replace("xsd:", ""))
        #~ self.ui.gridLayout.addWidget(qlabelname, self.columnid, 0)
        #~ self.ui.gridLayout.addWidget(qlineedit, self.columnid, 1)
        #~ self.ui.gridLayout.addWidget(qlabeltype, self.columnid, 2)
        #~ self.columnid = self.columnid + 1
        #~ self.parameter_labels.append(qlabelname)
        #~ self.parameter_labels.append(qlabeltype)
        #~ self.parameter_lineedits.append(qlineedit)
        #~ # newHeight = self.geometry().height() + 21
        #~ # self.resize(self.geometry().width(), newHeight)
#~ 
#~ 
    #~ def lock_ui(self):
        #~ self.ui.cmdGetCapabilities.setEnabled(False)
        #~ self.ui.cmdListStoredQueries.setEnabled(False)
        #~ self.ui.cmdGetFeature.setEnabled(False)
        #~ self.ui.cmbFeatureType.setEnabled(False)
        #~ self.show_metadata_button(False)
        #~ self.show_extent_button(False)
#~ 
    #~ def unlock_ui(self):
        #~ self.ui.cmdGetCapabilities.setEnabled(True)
        #~ self.ui.cmdListStoredQueries.setEnabled(True)
        #~ self.ui.cmdGetFeature.setEnabled(True)
        #~ self.ui.cmbFeatureType.setEnabled(True)
        #~ self.show_metadata_button(True)
        #~ self.show_extent_button(True)
#~ 
    #~ def show_metadata_button(self, enabled):
        #~ if enabled:
            #~ if self.querytype == "adhocquery":
                #~ featuretype = self.featuretypes[self.ui.cmbFeatureType.currentText()]
                #~ if featuretype.getMetadataUrl():
                    #~ if len(featuretype.getMetadataUrl()) > 0:
                        #~ self.ui.cmdMetadata.setEnabled(True)
                    #~ else:
                        #~ self.ui.cmdMetadata.setEnabled(False)
                #~ else:
                    #~ self.ui.cmdMetadata.setEnabled(False)
        #~ else:
            #~ self.ui.cmdMetadata.setEnabled(False)
#~ 
    #~ def show_extent_button(self, enabled):
        #~ if enabled:
            #~ if self.querytype == "adhocquery":
                #~ featuretype = self.featuretypes[self.ui.cmbFeatureType.currentText()]
                #~ if featuretype.getWgs84BoundingBoxEast():
                    #~ if featuretype.getWgs84BoundingBoxEast() > 0:
                        #~ self.ui.cmdExtent.setEnabled(True)
                    #~ else:
                        #~ self.ui.cmdExtent.setEnabled(False)
                #~ else:
                    #~ self.ui.cmdExtent.setEnabled(False)
        #~ else:
            #~ self.ui.cmdExtent.setEnabled(False)
#~ 
#~ 
    #~ def show_extent(self):
        #~ featuretype = self.featuretypes[self.ui.cmbFeatureType.currentText()]
        #~ self.create_layer(featuretype)
#~ 
#~ 
    #~ def create_layer(self, featuretype):
        #~ layer = QgsVectorLayer("polygon?crs=epsg:4326&", featuretype.getName() + " (Extent)", "memory")
        #~ QgsMapLayerRegistry.instance().addMapLayer(layer)
#~ 
        #~ e = featuretype.getWgs84BoundingBoxEast()
        #~ s = featuretype.getWgs84BoundingBoxSouth()
        #~ w = featuretype.getWgs84BoundingBoxWest()
        #~ n = featuretype.getWgs84BoundingBoxNorth()
#~ 
        #~ wkt = "POLYGON((" + e + " " + s + "," + e + " " + n + "," + w + " " + n + "," + w + " " + s + "," + e + " " + s + "))"
        #~ geom = QgsGeometry.fromWkt(wkt)
        #~ feature = QgsFeature()
        #~ feature.setGeometry(geom)
#~ 
        #~ features = [feature]
        #~ layer.dataProvider().addFeatures(features)
        #~ layer.updateExtents()
        #~ layer.reload()
        #~ self.parent.iface.mapCanvas().refresh()
        #~ self.parent.iface.zoomToActiveLayer()
#~ 
#~ 
    #~ def show_metadata(self):
        #~ featuretype = self.featuretypes[self.ui.cmbFeatureType.currentText()]
        #~ xslfilename = os.path.join(plugin_path, "iso19139jw.xsl")
#~ 
        #~ html = self.xsl_transform(featuretype.getMetadataUrl(), xslfilename)
#~ 
        #~ if html:
            #~ # create and show the dialog
            #~ dlg = MetadataClientDialog()
            #~ dlg.ui.wvMetadata.setHtml(html)
            #~ # show the dialog
            #~ dlg.show()
            #~ result = dlg.exec_()
            #~ # See if OK was pressed
            #~ if result == 1:
                #~ # do something useful (delete the line containing pass and
                #~ # substitute with your code
                #~ pass
        #~ else:
            #~ QtGui.QMessageBox.critical(self, "Metadata Error", "Unable to read the Metadata")
#~ 
#~ 
#~ 
    #~ """
    #~ ############################################################################################################################
    #~ # UTIL
    #~ ############################################################################################################################
    #~ """
#~ 
    #~ def logMessage(self, message):
        #~ if globals().has_key('QgsMessageLog'):
            #~ QgsMessageLog.logMessage(message, "Wfs20Client")
#~ 
    #~ def get_url(self):
        #~ defaultwfs = self.settings.value("/Wfs20Client/defaultWfs")
        #~ if defaultwfs:
            #~ return defaultwfs
        #~ else:
            #~ return "http://geoserv.weichand.de:8080/geoserver/wfs"
#~ 
    #~ def get_featurelimit(self):
        #~ defaultfeaturelimit = self.settings.value("/Wfs20Client/defaultFeatureLimit")
        #~ if defaultfeaturelimit:
            #~ return defaultfeaturelimit
        #~ else:
            #~ return "1000"
#~ 
    #~ def get_temppath(self, filename):
        #~ tmpdir = os.path.join(tempfile.gettempdir(),'wfs20client')
        #~ if not os.path.exists(tmpdir):
            #~ os.makedirs(tmpdir)
        #~ tmpfile= os.path.join(tmpdir, filename)
        #~ return tmpfile
#~ 
    #~ # Receive Proxy from QGIS-Settings
    #~ def getProxy(self):
#~ 
        #~ excluded_urls = self.settings.value("/proxy/proxyExcludedUrls")
        #~ if excluded_urls:
            #~ for excluded_url in excluded_urls.split("|"):
                #~ if excluded_url in self.onlineresource:
                    #~ return ""
#~ 
        #~ if self.settings.value("/proxy/proxyEnabled") == "true":
           #~ proxy = "{0}:{1}".format(self.settings.value("/proxy/proxyHost"), self.settings.value("/proxy/proxyPort"))
           #~ if proxy.startswith("http://"):
               #~ return proxy
           #~ else:
               #~ return proxy
        #~ else:
            #~ return ""
#~ 
    #~ # Setup urllib2 (Proxy)
    #~ def setup_urllib2(self, request, username, password):
#~ 
        #~ # Proxy-Handler
        #~ if not self.getProxy() == "":
            #~ if (request.startswith("https")):
                #~ proxy_handler = urllib2.ProxyHandler({"https" : self.getProxy()})
            #~ else:
                #~ proxy_handler = urllib2.ProxyHandler({"http" : self.getProxy()})
        #~ else:
            #~ proxy_handler = urllib2.ProxyHandler({})
#~ 
        #~ # Auth-Handler
        #~ if username and len(username) > 0 and password and len(password) > 0:
#~ 
            #~ # Auth-Handler
            #~ password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
            #~ password_mgr.add_password(None, request, username, password)
            #~ auth_handler = HTTPBasicAuthHandlerLimitRetries(password_mgr)
            #~ opener = urllib2.build_opener(proxy_handler, auth_handler)
#~ 
        #~ else:
            #~ opener = urllib2.build_opener(proxy_handler)
#~ 
        #~ urllib2.install_opener(opener)
#~ 
#~ 
    #~ # Setup Qhttp (Proxy)
    #~ def setup_qhttp(self):
        #~ self.http = QHttp(self)
        #~ if not self.getProxy() == "":
            #~ self.http.setProxy(QgsNetworkAccessManager.instance().fallbackProxy()) # Proxy
#~ 
#~ 
    #~ # XSL Transformation
    #~ def xsl_transform(self, url, xslfilename):
        #~ try:
            #~ self.setup_urllib2(url, "", "")
            #~ response = urllib2.urlopen(url, None, 10)
            #~ encoding=response.headers['content-type'].split('charset=')[-1]
            #~ xml_source = unicode(response.read(), encoding)
        #~ except urllib2.HTTPError, e:
            #~ QtGui.QMessageBox.critical(self, "HTTP Error", "HTTP Error: {0}".format(e.code))
        #~ except urllib2.URLError, e:
            #~ QtGui.QMessageBox.critical(self, "URL Error", "URL Error: {0}".format(e.reason))
        #~ else:
            #~ # load xslt
            #~ xslt_file = QtCore.QFile(xslfilename)
            #~ xslt_file.open(QtCore.QIODevice.ReadOnly)
            #~ xslt = unicode(xslt_file.readAll())
            #~ xslt_file.close()
#~ 
            #~ # xslt
            #~ qry = QtXmlPatterns.QXmlQuery(QtXmlPatterns.QXmlQuery.XSLT20)
            #~ qry.setFocus(xml_source)
            #~ qry.setQuery(xslt)
#~ 
            #~ xml_target = qry.evaluateToString()
            #~ return xml_target
#~ 
#~ 
    #~ # WFS 2.0 UTILS
#~ 
    #~ # check for OWS-Exception
    #~ def is_exception(self, root):
        #~ for namespace in ["{http://www.opengis.net/ows}", "{http://www.opengis.net/ows/1.1}"]:
        #~ # check correct Rootelement
            #~ if root.tag == "{0}ExceptionReport".format(namespace):
                #~ for exception in root.findall("{0}Exception".format(namespace)):
                    #~ for exception_text in exception.findall("{0}ExceptionText".format(namespace)):
                        #~ QtGui.QMessageBox.critical(self, "OWS Exception", "OWS Exception returned from the WFS:<br>"+ str(exception_text.text))
                        #~ self.ui.lblMessage.setText("")
                #~ return True
        #~ return False
#~ 
#~ 
    #~ # check for correct WFS version (only WFS 2.0 supported)
    #~ def is_wfs20_capabilties(self, root):
        #~ if self.is_exception(root):
            #~ return False
        #~ if root.tag == "{0}WFS_Capabilities".format("{http://www.opengis.net/wfs/2.0}"):
            #~ return True
        #~ if root.tag == "{0}WFS_Capabilities".format("{http://www.opengis.net/wfs}"):
            #~ QtGui.QMessageBox.warning(self, "Wrong WFS Version", "This Plugin has dedicated support for WFS 2.0!")
            #~ self.ui.lblMessage.setText("")
            #~ return False
        #~ QtGui.QMessageBox.critical(self, "Error", "Not a valid WFS GetCapabilities-Response!")
        #~ self.ui.lblMessage.setText("")
        #~ return False
#~ 
#~ 
    #~ # Check for empty GetFeature result
    #~ def is_empty_response(self, root):
        #~ # deegree 3.2: numberMatched="unknown" does return numberReturned="0" instead of numberReturned="unknown"
        #~ # https://portal.opengeospatial.org/files?artifact_id=43925
        #~ if not root.get("numberMatched") == "unknown":
            #~ # no Features returned?
            #~ if root.get("numberReturned") == "0":
                #~ return True
        #~ return False
#~ 
#~ 
    #~ # Hack to fix version/acceptversions Request-Parameter
    #~ def fix_acceptversions(self, onlineresource, connector):
        #~ return "{0}service=WFS&acceptversions=2.0.0&request=GetCapabilities".format(connector)
#~ 
#~ 
    #~ # Determine namespaces in the capabilities (including non-used)
    #~ def get_namespace_map(self, xml):
        #~ nsmap = {}
        #~ for i in [m.start() for m in re.finditer('xmlns:', xml)]:
            #~ j = i + 6
            #~ prefix = xml[j:xml.find("=", j)]
            #~ k = xml.find("\"", j)
            #~ uri = xml[k + 1:xml.find("\"", k + 1)]
#~ 
            #~ prefix = prefix.strip()
            #~ # uri = uri.replace("\"","")
            #~ uri = uri.strip()
            #~ # text+= prefix + " " + uri + "\n"
#~ 
            #~ nsmap[prefix] = uri
        #~ return nsmap
#~ 
#~ 
    #~ #############################################################################################################
    #~ # QHttp GetFeature-Request - http://stackoverflow.com/questions/6852038/threading-in-pyqt4
    #~ #############################################################################################################
#~ 
    #~ # QHttp Slot
    #~ def downloadFile(self, onlineResource, queryString, fileName):
        #~ self.lock_ui()
        #~ url = QtCore.QUrl(onlineResource)
        #~ if QtCore.QFile.exists(fileName):
            #~ QtCore.QFile.remove(fileName)
#~ 
        #~ self.outFile = QtCore.QFile(fileName)
        #~ if not self.outFile.open(QtCore .QIODevice.WriteOnly):
            #~ QtGui.QMessageBox.information(self, 'Error', 'Unable to save the file %s: %s.' % (fileName, self.outFile.errorString()))
            #~ self.outFile = None
            #~ return
#~ 
        #~ port = url.port()
        #~ if port == -1:
            #~ port = 0
#~ 
        #~ if onlineResource.startswith("https"):
            #~ self.http.setHost(url.host(), QHttp.ConnectionModeHttps, port)
        #~ else:
            #~ self.http.setHost(url.host(), QHttp.ConnectionModeHttp, port)
#~ 
        #~ self.httpRequestAborted = False
        #~ # Download the file.
        #~ self.ui.progressBar.setVisible(True)
        #~ self.httpGetId = self.http.get(url.path() + queryString, self.outFile)
#~ 
#~ 
    #~ # Currently unused
    #~ def cancelDownload(self):
        #~ self.httpRequestAborted = True
        #~ self.http.abort()
        #~ self.close()
#~ 
        #~ self.ui.progressBar.setMaximum(1)
        #~ self.ui.progressBar.setValue(0)
        #~ self.unlock_ui()
#~ 
    #~ # QHttp Slot
    #~ def httpRequestFinished(self, requestId, error):
        #~ if requestId != self.httpGetId:
            #~ return
#~ 
        #~ if self.httpRequestAborted:
            #~ if self.outFile is not None:
                #~ self.outFile.close()
                #~ self.outFile.remove()
                #~ self.outFile = None
            #~ return
#~ 
        #~ self.outFile.close()
#~ 
        #~ self.ui.progressBar.setMaximum(1)
        #~ self.ui.progressBar.setValue(1)
#~ 
        #~ if error:
            #~ self.outFile.remove()
            #~ QtGui.QMessageBox.critical(self, "Error", "Download failed: %s." % self.http.errorString())
        #~ else:
            #~ # Parse and check only small files
            #~ if os.path.getsize(str(self.outFile.fileName())) < 5000:
                #~ root = ElementTree.parse(str(self.outFile.fileName())).getroot()
                #~ if not self.is_exception(root):
                    #~ if not self.is_empty_response(root):
                        #~ self.load_vector_layer(str(self.outFile.fileName()), self.ui.cmbFeatureType.currentText())
                    #~ else:
                        #~ QtGui.QMessageBox.information(self, "Information", "0 Features returned!")
                        #~ self.ui.lblMessage.setText("")
            #~ else:
                #~ self.load_vector_layer(str(self.outFile.fileName()), self.ui.cmbFeatureType.currentText())
#~ 
        #~ self.ui.progressBar.setMaximum(1)
        #~ self.ui.progressBar.setValue(0)
        #~ self.unlock_ui()
#~ 
    #~ # QHttp Slot
        #~ # Check for genuine error conditions.Gz
    #~ def readResponseHeader(self, responseHeader):
        #~ if responseHeader.statusCode() not in (200, 300, 301, 302, 303, 307):
            #~ QtGui.QMessageBox.critical(self, 'Error',
                    #~ 'Download failed: %s.' % responseHeader.reasonPhrase())
            #~ self.ui.lblMessage.setText("")
            #~ self.httpRequestAborted = True
            #~ self.http.abort()
#~ 
    #~ def updateDataReadProgress(self, bytesRead, totalBytes):
        #~ if self.httpRequestAborted:
            #~ return
        #~ self.ui.progressBar.setMaximum(totalBytes)
        #~ self.ui.progressBar.setValue(bytesRead)
        #~ self.ui.lblMessage.setText("Please wait while downloading - {0} Bytes downloaded!".format(str(bytesRead)))
#~ 
    #~ # QHttp Slot
    #~ def authenticationRequired(self, hostName, _, authenticator):
        #~ authenticator.setUser(self.ui.txtUsername.text().strip())
        #~ authenticator.setPassword(self.ui.txtPassword.text().strip())
#~ 
    #~ def sslErrors(self, errors):
        #~ errorString = ""
        #~ for error in errors:
            #~ errorString+=error.errorString() + "\n"
        #~ # QtGui.QMessageBox.critical(self, "Error", errorString)
#~ 
        #~ self.http.ignoreSslErrors()
#~ 
    #~ def load_vector_layer(self, filename, layername):
#~ 
        #~ self.ui.lblMessage.setText("Loading GML - Please wait!")
        #~ self.logger.debug("### LOADING GML ###")
#~ 
        #~ # Configure OGR/GDAL GML-Driver
        #~ resolvexlinkhref = self.settings.value("/Wfs20Client/resolveXpathHref")
        #~ attributestofields = self.settings.value("/Wfs20Client/attributesToFields")
        #~ disablenasdetection = self.settings.value("/Wfs20Client/disableNasDetection")
#~ 
        #~ gdaltimeout = "5"
        #~ self.logger.debug("GDAL_HTTP_TIMEOUT " + gdaltimeout)
        #~ gdal.SetConfigOption("GDAL_HTTP_TIMEOUT", gdaltimeout)
        #~ if resolvexlinkhref and resolvexlinkhref == "true":
            #~ gdal.SetConfigOption('GML_SKIP_RESOLVE_ELEMS', 'NONE')
            #~ self.logger.debug("resolveXpathHref " + resolvexlinkhref)
        #~ else:
            #~ gdal.SetConfigOption('GML_SKIP_RESOLVE_ELEMS', 'ALL')
#~ 
        #~ if attributestofields and attributestofields == "true":
            #~ gdal.SetConfigOption('GML_ATTRIBUTES_TO_OGR_FIELDS', 'YES')
            #~ self.logger.debug("attributesToFields " + attributestofields)
        #~ else:
            #~ gdal.SetConfigOption('GML_ATTRIBUTES_TO_OGR_FIELDS', 'NO')
#~ 
        #~ nasdetectionstring = "NAS-Operationen.xsd;NAS-Operationen_optional.xsd;AAA-Fachschema.xsd"
        #~ if not disablenasdetection or disablenasdetection == "true":
            #~ nasdetectionstring = 'asdf/asdf/asdf'
        #~ self.logger.debug("Using 'NAS_INDICATOR': " + nasdetectionstring)
        #~ gdal.SetConfigOption('NAS_INDICATOR', nasdetectionstring)
#~ 
        #~ # Analyse GML-File
        #~ ogrdriver = ogr.GetDriverByName("GML")
        #~ self.logger.debug("OGR Datasource: " + filename)
        #~ ogrdatasource = ogrdriver.Open(filename)
        #~ self.logger.debug("... loaded")
#~ 
        #~ if ogrdatasource is None:
            #~ QtGui.QMessageBox.critical(self, "Error", "Response is not a valid QGIS-Layer!")
            #~ self.ui.lblMessage.setText("")
#~ 
        #~ else:
            #~ # Determine the LayerCount
            #~ ogrlayercount = ogrdatasource.GetLayerCount()
            #~ self.logger.debug("OGR LayerCount: " + str(ogrlayercount))
#~ 
            #~ hasfeatures = False
#~ 
#~ 
            #~ # Load every Layer
            #~ for i in range(0, ogrlayercount):
#~ 
                #~ j = ogrlayercount -1 - i # Reverse Order?
                #~ ogrlayer = ogrdatasource.GetLayerByIndex(j)
                #~ ogrlayername = ogrlayer.GetName()
                #~ self.logger.debug("OGR LayerName: " + ogrlayername)
#~ 
                #~ ogrgeometrytype = ogrlayer.GetGeomType()
                #~ self.logger.debug("OGR GeometryType: " + ogr.GeometryTypeToName(ogrgeometrytype))
#~ 
                #~ geomtypeids = []
#~ 
                #~ # Abstract Geometry
                #~ if ogrgeometrytype==0:
                    #~ self.logger.debug("AbstractGeometry-Strategy ...")
                    #~ geomtypeids = ["1", "2", "3", "100"]
#~ 
                #~ # One GeometryType
                #~ else:
                    #~ self.logger.debug("DefaultGeometry-Strategy ...")
                    #~ geomtypeids = [str(ogrgeometrytype)]
#~ 
#~ 
                #~ # Create a Layer for each GeometryType
                #~ for geomtypeid in geomtypeids:
#~ 
                    #~ qgislayername = ogrlayername # + "#" + filename
                    #~ uri = filename + "|layerid=" + str(j)
#~ 
                    #~ if len(geomtypeids) > 1:
                        #~ uri += "|subset=" + self.getsubset(geomtypeid)
#~ 
                    #~ self.logger.debug("Loading QgsVectorLayer: " + uri)
                    #~ vlayer = QgsVectorLayer(uri, qgislayername, "ogr")
                    #~ vlayer.setProviderEncoding("UTF-8") #Ignore System Encoding --> TODO: Use XML-Header
#~ 
                    #~ if not vlayer.isValid():
                        #~ QtGui.QMessageBox.critical(self, "Error", "Response is not a valid QGIS-Layer!")
                        #~ self.ui.lblMessage.setText("")
                    #~ else:
                        #~ featurecount = vlayer.featureCount()
                        #~ if featurecount > 0:
                            #~ hasfeatures = True
                            #~ QgsMapLayerRegistry.instance().addMapLayers([vlayer])
                            #~ self.logger.debug("... added Layer! QgsFeatureCount: " + str(featurecount))
                            #~ #self.parent.iface.mapCanvas().zoomToFullExtent()
#~ 
#~ 
            #~ if hasfeatures == False:
                #~ QtGui.QMessageBox.information(self, "Information", "No Features returned!")
#~ 
            #~ self.ui.lblMessage.setText("")
#~ 
#~ 
#~ 
    #~ def getsubset(self, geomcode):
#~ 
        #~ if      geomcode == "1":    return "OGR_GEOMETRY='POINT' OR OGR_GEOMETRY='MultiPoint'"
        #~ elif    geomcode == "2":    return "OGR_GEOMETRY='LineString' OR OGR_GEOMETRY='MultiLineString'"
        #~ elif    geomcode == "3":    return "OGR_GEOMETRY='Polygon' OR OGR_GEOMETRY='MultiPolygon'"
        #~ elif    geomcode == "100":  return "OGR_GEOMETRY='None'"
        #~ else:                       return "OGR_GEOMETRY='Unknown'"
#~ 
#~ 
#~ 
#~ ###
#~ #
#~ # Custom HTTPBasicAuthHandler with fix for infinite retries when submitting wrong password
#~ # http://bugs.python.org/issue8797
#~ # http://bugs.python.org/file20471/simpler_fix.patch
#~ #
#~ ###
#~ 
#~ class HTTPBasicAuthHandlerLimitRetries(urllib2.HTTPBasicAuthHandler):
    #~ def __init__(self, *args, **kwargs):
        #~ urllib2.HTTPBasicAuthHandler.__init__(self, *args, **kwargs)
#~ 
    #~ def http_error_auth_reqed(self, authreq, host, req, headers):
        #~ authreq = headers.get(authreq, None)
        #~ if authreq:
            #~ mo = urllib2.AbstractBasicAuthHandler.rx.search(authreq)
            #~ if mo:
                #~ if len(mo.groups()) == 3:
                    #~ scheme, quote, realm = mo.groups()
                #~ else:
                    #~ scheme, realm = mo.groups()
                #~ if scheme.lower() == 'basic':
                    #~ return self.retry_http_basic_auth(host, req, realm)
#~ 
    #~ def retry_http_basic_auth(self, host, req, realm):
        #~ user, pw = self.passwd.find_user_password(realm, host)
        #~ if pw is not None:
            #~ raw = "%s:%s" % (user, pw)
            #~ auth = 'Basic %s' % urllib2.base64.b64encode(raw).strip()
            #~ if req.get_header(self.auth_header, None) == auth:
                #~ return None
            #~ req.add_unredirected_header(self.auth_header, auth)
            #~ #return self.parent.open(req, timeout=req.timeout)
            #~ return self.parent.open(req)
