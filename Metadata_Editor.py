#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from qgis.core import *

from .ME_19115_200 import ME_19115_200

import os, sys

# ----- editor ---------------------------------------------------------

class editor:
  def __init__(self, iface):
    # save reference to the QGIS interface
    self.iface = iface
    # bepaal start directorie en bestand
    self.start_dir, self.start_file = os.path.split(os.path.abspath(__file__))

  def initGui(self):
    # acties om ISO 19115 2.0.0 op te starten
    icon = QIcon("%s/images/Metadata_Editor.png" %(self.start_dir))
    self.ActionME_19115_200 = QAction(icon, "&ME 19115 2.0.0", self.iface.mainWindow())
    self.ActionME_19115_200.triggered.connect(self.RunME_19115_200)
    self.ActionME_19115_200.setCheckable(False)
    # add toolbar button and menu item
    self.iface.addToolBarIcon(self.ActionME_19115_200)
    self.iface.addPluginToMenu("&Metadata Editor", self.ActionME_19115_200)

    # acties om ISO 19110 2016 op te starten
    icon = QIcon("%s/images/Metadata_Editor.png" %(self.start_dir))
    self.ActionME_19110_2016 = QAction(icon, "&ME 19110 2016", self.iface.mainWindow())
    self.ActionME_19110_2016.triggered.connect(self.RunME_19110_2016)
    self.ActionME_19110_2016.setCheckable(False)
    # add toolbar button and menu item
    self.iface.addToolBarIcon(self.ActionME_19110_2016)
    self.iface.addPluginToMenu("&Metadata Editor", self.ActionME_19110_2016)

    # acties om about op te starten
    icon = QIcon("%s/images/help.png" %(self.start_dir))
    self.AboutAction = QAction(icon, "over Metadata Editor", self.iface.mainWindow())
    self.AboutAction.triggered.connect(self.RunAbout)    
    self.AboutAction.setCheckable(False)
    # add toolbar button and menu item
    self.iface.addToolBarIcon(self.AboutAction)
    self.iface.addPluginToMenu("&Metadata Editor", self.AboutAction)

  def unload(self):
    "Remove the plugin menu item and icon from QGIS GUI."
    self.iface.removePluginMenu("Metadata Editor", self.ActionME_19115_200)
    self.iface.removePluginVectorMenu("Metadata Editor", self.ActionME_19110_2016)
    self.iface.removePluginVectorMenu("Metadata Editor", self.AboutAction)
    self.iface.removeToolBarIcon(self.ActionME_19115_200)
    self.iface.removeToolBarIcon(self.ActionME_19110_2016)
    self.iface.removeToolBarIcon(self.AboutAction)

  def RunME_19115_200(self):
    # als er geen actieve laag is geef een popup
    if not self.iface.activeLayer():
      QtWidgets.QMessageBox.warning(None, "Laag Info", "Er is geen laag actief,\nselecteer een laag")      
    elif 'url=' in self.iface.activeLayer().dataProvider().dataSourceUri():
      QtWidgets.QMessageBox.warning(None, "Laag Info", "Metadata Editor ISO 19115 versie 2.0.0\nis niet geschikt voor webservices\ngebruik ISO 19119.") 
    else:
      # create and show the dialog
      dlg = ME_19115_200(self)
      # show the dialog
      dlg.show()
      result = dlg.exec_()

  def RunME_19110_2016(self):
    pass
    # ~ # als er geen actieve laag is geef een popup
    # ~ if not self.iface.activeLayer():
      # ~ QtWidgets.QMessageBox.warning(None, "Laag Info", "Er is geen laag actief,\nselecteer een laag")      
    # ~ else:
      # ~ # create and show the dialog
      # ~ dlg = ME_Dialog(self)
      # ~ # show the dialog
      # ~ dlg.show()
      # ~ result = dlg.exec_()
    
  def RunAbout(self):
    infoString = """<table width="510">
    <tr><td colspan=\"2\"><b><font size="6">Metadata_Editor 0.9.0 beta</font></b></td></tr>
    <tr><td></td></tr>
    <tr><td align="center"><IMG SRC="%s/images/PNB_logo.jpg" ALT="onbekend" WIDTH=16 HEIGHT=16></td></tr>
    <tr><td>Metadata_Editor is een programma voor het</td></tr>
    <tr><td>aanmaken van GEO metadata, die voldoet aan</td></tr>
    <tr><td>de door Geonovum vastgestelde normen</td></tr>
    <tr><td></td></tr>
    <tr><td><font size="2">&copy; 2018    Jan van Sambeek</font></td></tr></table>""" %(self.start_dir)
    QMessageBox.information(self.iface.mainWindow(), "over Metadata Editor", infoString)

# ----------------------------------------------------------------------
